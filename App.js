import React, { Component } from 'react';
import { Text, View, Dimensions, Button,StyleSheet, TouchableOpacity } from 'react-native';
import MapView, { Marker } from "react-native-maps";
import Geolocation from '@react-native-community/geolocation';
import { PermissionsAndroid } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
const { width, height  } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.006339428281933124;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
// import requestLocationPermission from './Test';

// Disable yellow box warning messages
console.disableYellowBox = true;


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
     latitude: 25.0927568, 
     longitude: 55.1593013,
     selectedSite:'1',
     sites:[
          { label: 'Site 1', value: '1' },
          { label: 'Site 2', value: '2' },
          { label: 'Site 3', value: '3' },
      ]
  }
  this.mapRef = React.createRef();
  
  this.indx = 0;
}
async requestLocationPermission() 
{
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        'title': 'Example App',
        'message': 'Example App access to your location '
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the location")
      // alert("You can use the location");
    } else {
      console.log("location permission denied")
      // alert("Location permission denied");
    }
  } catch (err) {
    console.warn(err)
  }
}

async componentWillMount(){
  await this.requestLocationPermission()
      this.watchId = Geolocation.watchPosition(
        (position) => {
          const markerCoord = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          }
          this.indx = this.indx + 1;  
          const duration = 100
          const region = {
            ...markerCoord,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA        
          }; 
          if(region !== null )  {
            console.log(this.mapRef);
            
            this.mapRef.animateToRegion(region,1000*2)
            this.setState({region:region})
          }  
          
          
        },
        (error) => {
          console.log(error);
          
        },
        {enableHighAccuracy:false,timeout:1,maximumAge:1,distanceFilter:1}
      )
}

  render() {

    let initialRegion = {
      latitude:41.068038,
      longitude:29.061284,
      latitudeDelta:0.01,
      longitudeDelta:0.01
    }

    let myLocation ={latitude:this.state.latitude,longitude:this.state.longitude}
    let myLocation2 ={latitude:this.state.latitude+0.0020,longitude:this.state.longitude+0.0001}
    let myLocation3 ={latitude:this.state.latitude+0.0010,longitude:this.state.longitude+0.0030}
    let myLocation4 ={latitude:this.state.latitude+0.0005,longitude:this.state.longitude}
    let myLocation5 ={latitude:this.state.latitude,longitude:this.state.longitude+0.0020}
    let myLocation6 ={latitude:this.state.latitude+0.0015,longitude:this.state.longitude+0.0010}
    let myLocation7 ={latitude:this.state.latitude,longitude:this.state.longitude+0.0020}
    let myCoordinate = {latitude:41.068038,longitude:29.061824}
    console.log("ffffff",this.state.selectedSite == 2);
    
      return (
        <View style={styles.container}>
                  <MapView
               ref={(ref) => this.mapRef = ref}
              style={styles.map}
              showsUserLocation={true}
              onMapReady={this.onMapReady}
              onRegionChangeComplete={this.onRegionChange}>
            <MapView.Marker
              pinColor={"green"}
              coordinate={myLocation}
              title={"Your Location"}
              draggable />
              {this.state.selectedSite == 2
              ?
              <View>
                <MapView.Marker
              pinColor={"red"}
              coordinate={myLocation2}
              title={"Your Location"}
              draggable />
               <MapView.Marker
              pinColor={"red"}
              coordinate={myLocation3}
              title={"Your Location"}
              draggable />
               <MapView.Marker
              pinColor={"red"}
              coordinate={myLocation4}
              title={"Your Location"}
              draggable />
              </View>
              :
              <View>
              <MapView.Marker
              pinColor={"red"}
              coordinate={myLocation5}
              title={"Your Location"}
              draggable />
               <MapView.Marker
              pinColor={"red"}
              coordinate={myLocation6}
              title={"Your Location"}
              draggable />
               <MapView.Marker
              pinColor={"red"}
              coordinate={myLocation7}
              title={"Your Location"}
              draggable />
              </View>
              }
              {this.state.selectedSite == 3
              ?
              <MapView.Marker
              pinColor={"red"}
              coordinate={myLocation3}
              title={"Your Location"}
              draggable />
              :
              <View/>
              }
             
                    <MapView.Circle 
                        key = { 'test' }
                        center = { myLocation}
                        radius = {200}
                        strokeWidth = { 2 }
                        strokeColor = { '#1a66ff' }
                        fillColor = { 'rgba(230,238,255,0.5)' }
                                />
                      </MapView>

                      <View
                          style={{
                              position: 'absolute',//use absolute position to show button on top of the map
                              top: '5%', //for center align
                              width:"90%",
                              height:"8%",
                              alignSelf: 'center',

                              backgroundColor:"whitesmoke",
                              borderColor:"grey",
                              borderWidth:1,
                              borderRadius:12,
                              justifyContent:"center" //for align to right
                          }}
                          >
                            <RNPickerSelect
            onValueChange={(value) => console.log(value)}
            style={{backgroundColor:"grey"}}
            onValueChange={(value,index) => this.setState({selectedSite:value})}
            value={this.state.selectedSite}
            items={this.state.sites}
        />
                      </View>
                      <View  style={{
                              position: 'absolute',//use absolute position to show button on top of the map
                              bottom: '5%', //for center align
                              width:"30%",
                              height:"8%",
                              alignSelf: 'center',
                              justifyContent:"center" //for align to right
                          }}
                          >
                            <TouchableOpacity>

                             <Button title={"Puch In"} />
                            </TouchableOpacity>

                      </View>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:"center",
    alignItems:"center",
    backgroundColor:"#f5fcff"
  },
  map:{
    position:"absolute",
    left:0,
    top:0,
    right:0,
    bottom:0

  }
  
})